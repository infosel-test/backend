"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _express = require("express");

var _info = _interopRequireDefault(require("./info.controllers"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const router = (0, _express.Router)(); // /api/info

router.route('/').post(_info.default.getVariation);
var _default = router;
exports.default = _default;