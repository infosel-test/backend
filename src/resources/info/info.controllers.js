import { crudControllers } from '../../utils/crud'
import Info from './info.model'

export default crudControllers(Info)
