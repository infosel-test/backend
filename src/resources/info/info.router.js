import { Router } from 'express'
import controllers from './info.controllers'

const router = Router()

// /api/info
router
  .route('/')
  .post(controllers.getVariation)

export default router
