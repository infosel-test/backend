import { Router } from 'express'
import { me, updateMe } from './user.controllers'

const router = Router()

// /api/user
router.get('/', me)
router.put('/', updateMe)

export default router