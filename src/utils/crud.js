export const getVariation = () => async (req, res) => {
  const val = req.body.clave
  // min & max
  const min = val - (val * 0.10)
  const max = val + (val * 0.10)
  
  // random variation
  const variation = Math.random() * (max - min) + min
  
  if (!variation) {
    return res.status(400).end()
  }
  
  res.status(200).json({ variation })
}

export const getOne = model => async (req, res) => {
  try {
    const doc = await model
      .findOne({ email: 'dev@dev.co' })
      .lean()
      .exec()

    if (!doc) {
      return res.status(400).end()
    }

    res.status(200).json({ data: doc })
  } catch (e) {
    console.error(e)
    res.status(400).end()
  }
}

export const crudControllers = model => ({
  getVariation: getVariation(model),
  getOne: getOne(model)
})
